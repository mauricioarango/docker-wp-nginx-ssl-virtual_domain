
# DOCKER AND WORDPRESS

This project is a simplification of this [setup](https://github.com/urre/wordpress-nginx-docker-compose) by Urban Sandén and the tutorial series ["Docker for local web development, introduction: why should you care?"](https://tech.osteel.me/posts/docker-for-local-web-development-introduction-why-should-you-care), by Yannick Chenot.

## WHAT IT DOES
Sets up a basic development environment for wordpress with PHP, MySQL, PHPMyAdmin, NGINX proxy server for HTTP2 traffic, and Mailhog for server mailing. Optional to download basic theme with babel/webpack for sass and bundling.
During initial set up creates a new domain in hosts file and SSL certificates.

## CONTENT

This branch contains a stack running on Docker, which includes:

* An alpine container for Nginx;
* A container for WORDPRESS - PHP-FPM;
* A container for MySQL;
* A container for phpMyAdmin;
* A volume for MySQL data;
* Mailhog for email testing.
* Downloadable WP theme with webpack/babel 

## PRE-REQUISITES

Make sure [Docker Desktop for Mac or PC](https://www.docker.com/products/docker-desktop) is installed and running, or head [over here](https://docs.docker.com/install/) if you are a Linux user. You will also need a terminal running [Git](https://git-scm.com/).

This setup also uses localhost's port 80, so make sure it is available (for instnace, kill any mamp installation that you have running).

## DIRECTIONS

Everything can be controlled from the 'super' bash file. To initialize a new environment:

* update `.env.example`;
* Run the initializing command: `$ bash super init` , which will create the containers, create domain in hosts file, and create certificates;
* Start containers: `bash super restart`;
* If WP theme is needed: `bash super wp`.

* `$ bash super recreate`, which basically deletes the containers and rebuilds them again


## RESULTS

### Wordpress

The Wordpress application will be available at https://yourdomain.local

### PHPMyAdmin

 PHPMyAdmin is found at [http://localhost:8080](http://localhost:8080/)

### Database

The database data is persisted in its own directory through the volume `./data`.

### MailHog

Mailhog interface will be available at [mailHog](http://localhost:8025/)

## USE OF THE 'super' BASH FILE

* To view all the available commands enter: `$ bash super`

* Then you can try individual actions like: `$ bash super logs`, which shows all the docker logs


## FOR REFERNCE, USEFUL DOCKER-COMPOSE COMMANDS

The following commands are for reference only. All of this actions can be executed from the `super` bash file. 

To stop the containers:

```
$ docker-compose stop
```

To destroy the containers:

```
$ docker-compose down
```

To destroy the containers and the associated volumes:

```
$ docker-compose down -v
```

To remove everything, including the images:

```
$ docker-compose down -v --rmi all
```

To force the rebuilding of the containers

```
docker-compose up -d --force-recreate  --build
```